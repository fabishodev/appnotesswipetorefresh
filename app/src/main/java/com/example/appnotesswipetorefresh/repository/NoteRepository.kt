package com.example.appnotesswipetorefresh.repository

import com.example.appnotesswipetorefresh.data.model.Note
import com.example.appnotesswipetorefresh.data.model.NoteList

interface NoteRepository {
    suspend fun getNotes(): NoteList
    suspend fun saveNote(note: Note?):Note?
}