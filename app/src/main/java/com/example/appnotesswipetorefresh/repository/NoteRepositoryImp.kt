package com.example.appnotesswipetorefresh.repository

import com.example.appnotesswipetorefresh.data.local.LocalDataSource
import com.example.appnotesswipetorefresh.data.model.Note
import com.example.appnotesswipetorefresh.data.model.NoteList
import com.example.appnotesswipetorefresh.data.model.toNoteEntity
import com.example.appnotesswipetorefresh.data.remote.NoteDataSource

class NoteRepositoryImp (
    private val localDataSource: LocalDataSource,
    private val dataSource: NoteDataSource): NoteRepository {

        override suspend fun getNotes(): NoteList {
            dataSource.getNotes().data.forEach{ note ->
                localDataSource.saveNote(note.toNoteEntity())
            }
            return localDataSource.getNotes()
        }

        override suspend fun saveNote(note: Note?): Note? {
            return dataSource.saveNote(note)
        }

        //override suspend fun getNotes(): NoteList = dataSource.getNotes()
        //override suspend fun saveNote(note: Note?): Note? = dataSource.saveNote(note)

}