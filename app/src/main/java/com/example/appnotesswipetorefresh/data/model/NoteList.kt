package com.example.appnotesswipetorefresh.data.model

data class NoteList(
    val data:List<Note> = listOf()
)