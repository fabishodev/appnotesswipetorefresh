package com.example.appnotesswipetorefresh.data.remote

import com.example.appnotesswipetorefresh.data.model.Note
import com.example.appnotesswipetorefresh.data.model.NoteList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    ///https://testapi.io/api/fabishodev/resource/notes
    @GET("notes")
    suspend fun getNotes(): NoteList

    @POST("notes")
    suspend fun saveNote(@Body note: Note?):Note?
}