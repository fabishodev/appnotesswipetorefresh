package com.example.appnotesswipetorefresh.data.local

import com.example.appnotesswipetorefresh.data.model.NoteEntity
import com.example.appnotesswipetorefresh.data.model.NoteList
import com.example.appnotesswipetorefresh.data.model.toNoteList


    class LocalDataSource(private val noteDao: NoteDao) {

        suspend fun getNotes(): NoteList = noteDao.getNotes().toNoteList()
        suspend fun saveNote(note: NoteEntity) = noteDao.saveNote(note)

    }
