package com.example.appnotesswipetorefresh.ui.notes

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.appnotesswipetorefresh.R
import com.example.appnotesswipetorefresh.core.Resource
import com.example.appnotesswipetorefresh.data.local.AppDatabase
import com.example.appnotesswipetorefresh.data.local.LocalDataSource
import com.example.appnotesswipetorefresh.data.model.Note
import com.example.appnotesswipetorefresh.data.remote.ApiClient
import com.example.appnotesswipetorefresh.data.remote.NoteDataSource
import com.example.appnotesswipetorefresh.databinding.FragmentNotesBinding
import com.example.appnotesswipetorefresh.presentation.NoteViewModel
import com.example.appnotesswipetorefresh.presentation.NoteViewModelFactory
import com.example.appnotesswipetorefresh.repository.NoteRepositoryImp
import com.example.appnotesswipetorefresh.ui.notes.adapters.NoteAdapter


class NotesFragment : Fragment(R.layout.fragment_notes) {

    private lateinit var binding: FragmentNotesBinding
    private lateinit var adapter: NoteAdapter

    private val viewModel by viewModels<NoteViewModel>{
        NoteViewModelFactory(NoteRepositoryImp(
            LocalDataSource(AppDatabase.getDataBase(this.requireContext()).noteDao()),
            NoteDataSource(ApiClient.service)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("LiveData", "onViewCreated NOTESFRAGMENT")
        binding = FragmentNotesBinding.bind(view)
        binding.recyclerNotes.layoutManager  = GridLayoutManager(requireContext(), 2)

        binding.btnAddNote.setOnClickListener{
            val action = NotesFragmentDirections.actionNotesFragmentToNoteEditFragment()
            findNavController().navigate(action)
        }

        viewModel.fetchNotes().observe(viewLifecycleOwner, Observer { result ->
            when(result){
                is Resource.Loading -> {
                    binding.progressbar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressbar.visibility = View.GONE
                    adapter  = NoteAdapter(result.data.data){ note ->
                        onNoteClick(note)
                        //Toast.makeText(this.context,note.title,Toast.LENGTH_LONG).show()
                    }
                    binding.recyclerNotes.adapter = adapter
                    Log.d("LiveData","${result.data.toString()}")
                }
                is Resource.Failure -> {
                    binding.progressbar.visibility = View.GONE
                    Log.d("LiveData","${result.exception.toString()}")
                }
            }
        })

        binding.swipeContainer.setOnRefreshListener {
            Log.d("LiveData", "setOnRefreshListener")

            viewModel.fetchNotes().observe(viewLifecycleOwner, Observer { result ->
                when (result) {
                    is Resource.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.progressbar.visibility = View.GONE
                        adapter = NoteAdapter(result.data.data) { note ->
                            //Toast.makeText(this.context, note.title, Toast.LENGTH_LONG)
                            onNoteClick(note)
                        }
                        binding.recyclerNotes.adapter = adapter
                        Log.d("LiveData", "${result.data.toString()}")
                    }
                    is Resource.Failure -> {
                        binding.progressbar.visibility = View.GONE
                        Log.d("LiveData", "${result.exception.toString()}")
                    }
                }
            })
            //fetchDataAsync()
            binding.swipeContainer.setRefreshing(false)
        }

        // Configure the refreshing colors
        binding.swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light);
    }

    private fun onNoteClick(note: Note){
        val action = NotesFragmentDirections.actionNotesFragmentToNoteDetailFragment(
            note.id,
            note.title,
            note.content,
            note.image
        )

        findNavController().navigate(action)
    }

}